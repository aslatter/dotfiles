#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto -v'
alias grep='grep --color=auto'
PS1='[\u@\h \W]\$ '

# History setup
HISTCONTROL=ignoreboth
shopt -s histappend
HISTSIZE=100000
HISTFILESIZE=100000

# check window-size after each command
shopt -s checkwinsize

bind 'set enable-bracketed-paste on'

if [[ -d ~/.config/repo ]]; then
	alias config="git --work-tree=$HOME --git-dir=$HOME/.config/repo"
fi

if command -v fzf >/dev/null 2>&1; then
  eval "$(fzf --bash)"
fi

if command -v starship >/dev/null 2>&1; then
  eval "$(starship init bash)"
fi

# fzf + rg integration
if command -v fzf >/dev/null 2>&1 && command -v rg >/dev/null 2>&1; then
  export FZF_DEFAULT_COMMAND="rg --files --hidden '--ignore-file=$HOME/.fzf-rgignore'"
  export FZF_CTRL_T_COMMAND="$FZF_DEFAULT_COMMAND"
fi

# fuzzy jump-to-project
p() {
  dir=~/projects
  prj=$(find "$dir" -maxdepth 3 -name .git -type d -prune -printf "%P\n" | xargs dirname | fzf -1 --query="$*" --preview="ls -lah \"$dir/\"{}")
  if [ -n "$prj" ]; then
    cd "$dir/$prj"
  fi
}

# git aliases
function ga() {
  if [[ $# -gt 0 ]]; then
    git add "$@"
    return $?
  fi
  git add -p
}
alias gd="git diff"
alias gc="git commit -v"
alias gs="git status"
alias gl="git log"

# I'm too used to these from my work laptop
if command -v xsel >/dev/null 2>&1; then
  alias pbcopy='xsel --clipboard --input'
  alias pbpaste='xsel --clipboard --output'
fi

if command -v xdg-open >/dev/null 2>&1; then
  alias open=xdg-open
fi

if command -v paru >/dev/null 2>&1; then
  alias yay=paru
fi

# if command -v bat >/dev/null 2>&1; then
#   MANPAGER="sh -c 'col -bx | bat -l man -p'"
#   export MANPAGER
# fi

# various toolchains and path-extensions

# local scripts
if [[ -d "$HOME/scripts" ]]; then
  PATH="$HOME/scripts:$PATH"
fi

# rust
if [[ -d "$HOME/.cargo/bin" ]]; then
  PATH="$HOME/.cargo/bin:$PATH"
fi

# golang
if [[ -d "$HOME/go/bin" ]]; then
  PATH="$HOME/go/bin:$PATH"
fi

export PATH

export EDITOR=vim


[ -f "/home/antoine/.ghcup/env" ] && source "/home/antoine/.ghcup/env" # ghcup-env
