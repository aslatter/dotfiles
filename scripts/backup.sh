#!/bin/bash

set -eu

exclude_file="$HOME/.backup_exclude"

dest_file=$(mktemp --tmpdir "backup.$(date -I).XXXXXX.xz.sqfs")
cleanup() {
  rm -f "$dest_file"
}
trap cleanup EXIT

if ! [[ -f $dest_file ]]; then
  echo "Failed to create temp-file for new archive."
  exit 1
fi

rm -r "$dest_file"

echo "Creating backup in $dest_file ..."

tar --create \
  --checkpoint=.10000 \
  --preserve-permissions --xattrs --acls \
  --exclude-caches \
  --exclude-from="$exclude_file" \
  --exclude-vcs-ignores \
  --directory="$HOME" \
  . \
| sqfstar -comp xz -b 1M "$dest_file"

stat "$dest_file"

source ~/.backup_config

upload_dest_file="$B2_DEST_FOLDER/$(date --rfc-3339=date).xz.sqfs"

# Upload!
echo "Uploading $dest_file to $B2_BUCKET/$upload_dest_file ..."
b2 upload_file "$B2_BUCKET" "$dest_file" "$upload_dest_file"
