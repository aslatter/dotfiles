source $VIMRUNTIME/defaults.vim

" fix mouse
:set mouse=

unlet c_comment_strings

" Previous buffer
nnoremap <F7> :bp<CR>

" Next buffer
nnoremap <F8> :bn<CR>

" Spaces not tabs
:set tabstop=2
:set shiftwidth=2
:set expandtab

" Persistent undo
let vimdir = expand("~/.vim")
if !isdirectory(vimdir)
  call mkdir(vimdir)
endif

let undodir = expand("~/.vim/undo")
if !isdirectory(undodir)
  call mkdir(undodir)
endif

set undodir=~/.vim/undo
set undofile

